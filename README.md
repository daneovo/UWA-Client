
# unleashed-api

## Getting started
* Check out this repository
* `$ npm install`
* :tada: Start developing! :tada:

:warning: Remember to cover your code, and check this using `$ npm test` :warning:
