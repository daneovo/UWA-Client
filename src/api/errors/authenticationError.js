export class AuthenticationError extends Error {
  constructor(...args) {
    super(...args)
    Error.captureStackTrace(this, AuthenticationError)
  }
}
AuthenticationError.errorCode = 401
