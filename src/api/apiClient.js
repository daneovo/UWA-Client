import { AuthenticationError } from './errors/authenticationError.js'

export default class APIClient {

  constructor(baseUrl) {
    this.baseUrl = baseUrl
    Object.freeze(this)
  }

  async readFrom(url, params) {
    return this.accessResource(url, HTTPMethod.GET, params)
  }

  async postTo(url, params) {
    return this.accessResource(url, HTTPMethod.POST, params)
  }

  async putTo(url, params) {
    return this.accessResource(url, HTTPMethod.PUT, params)
  }

  async accessResource(path, method, params, headers) {
    const combinedUrl = this.baseUrl + path
    let fetchResource = fetch(combinedUrl, {
      method: method.name,
      headers: this.headers(method),
      body: JSON.stringify(params)
    })
    return fetchResource
    .then((response) => this.handleNotOk(response))
    .then((response) => {
      if (response.headers.has('Content-Type') && response.headers.get('Content-Type').includes('json')) {
        return response.json()
      }
      return response.body
    })
  }

  headers(httpMethod) {
    let headers = new Headers()
    headers.set('Accept', 'application/json')
    headers.set('Content-Type', httpMethod.type())

    return headers
  }

  handleFourOhOne(response) {
    return Promise.reject(new AuthenticationError(response))
  }

  handleNotOk(response) {
    if (this.isAccessDenied(response)){
      return this.handleFourOhOne(response)
    }

    if (!response.ok) {
      return Promise.reject(new Error(response))
    }
    return response
  }

  isAccessDenied(response) {
    return response.status == AuthenticationError.errorCode
  }
}

export class HTTPMethod {
  constructor(name) {
    this.name = name
  }

  type() {
    switch (this) {
      case HTTPMethod.GET:
      case HTTPMethod.PUT:
        return 'application/json'
      case HTTPMethod.POST:
        return 'application/x-www-form-urlencoded'
    }
  }
}

HTTPMethod.GET = new HTTPMethod('GET')
HTTPMethod.POST = new HTTPMethod('POST')
HTTPMethod.PUT = new HTTPMethod('PUT')
