import APIClient from "../src/api/apiClient.js"
import fetchMock from 'fetch-mock'
import fetch from 'isomorphic-fetch'

describe("APIClient", () => {
  let apiClient
  const baseUrl = 'http://somerandomstuff.com'
  const defaultPath = '/'

  beforeEach(() => {
    apiClient = new APIClient(baseUrl)
  })

  afterEach(() => {
    fetchMock.reset()
  })

  it('correctly sets the baseUrl', () => {
    expect(apiClient.baseUrl).toEqual(baseUrl)
  })

  describe('Network operations', () => {
    it('reads using GET', () => {
      mockGet()
      let promise = apiClient.readFrom(defaultPath, null)
      return expect(promise).resolves.toBeTruthy()
    })

    it('posts using POST', () => {
      mockPost()
      let promise = apiClient.postTo(defaultPath, null)
      return expect(promise).resolves.toBeTruthy()
    })

    it('posts using POST', () => {
      mockPut()
      let promise = apiClient.putTo(defaultPath, null)
      return expect(promise).resolves.toBeTruthy()
    })
  })

  function mockGet(url) {
    fetchMock.get(workingUrl(url), 200)
  }

  function mockPost(url) {
    fetchMock.post(workingUrl(url), 200)
  }

  function mockPut(url) {
    fetchMock.put(workingUrl(url), 200)
  }

  function workingUrl(url, path){
    if (!url) {
      url = baseUrl
    }
    if (!path) {
      url += defaultPath
    }
    return url
  }

})
