import APIClient from "../src/api/apiClient.js"
import AuthenticationError from "../src/api/errors/authenticationError.js"
import {Headers} from 'isomorphic-fetch'
import fetchMock from 'fetch-mock'

let apiClient

describe("APIClient", () => {

  beforeEach(() => {
    apiClient = new APIClient("derp")
    let mockObject = JSON.stringify({})
    fetchMock.mock("*", { number: 200, object: mockObject })
  })

  describe('Content-type is set correctly', () => {
    let jsonType = 'application/json'
    let postType = 'application/x-www-form-urlencoded'

    it('when reading', async () => {
      return clientRead().then( (value) => {
        verifyContentType(fetchMock.lastOptions().headers, jsonType)
      })
    });

    it('when posting', async () => {
      return clientPost().then( (value) => {
        verifyContentType(fetchMock.lastOptions().headers, postType)
      })
    });

    it('when putting', async () => {
      return clientPut().then( (value) => {
        verifyContentType(fetchMock.lastOptions().headers, jsonType)
      })
    });
  });

  function verifyContentType(headers, expectedType) {
    let contentTypeHeader = 'content-type'
    let actualType = headers.get(contentTypeHeader)
    expect(actualType).toBe(expectedType)
  }

  function clientRead(endpoint = "/") {
    return apiClient.readFrom(endpoint)
  }

  function clientPost(endpoint = "/") {
    return apiClient.postTo(endpoint)
  }

  function clientPut(endpoint = "/") {
    return apiClient.putTo(endpoint)
  }
})
