import APIClient from "../src/api/apiClient.js"
import AuthenticationError from "../src/api/errors/authenticationError.js"
import fetchMock from 'fetch-mock'
import fetch from 'isomorphic-fetch'

describe("APIClient", () => {
  let apiClient

  beforeEach(() => {
    apiClient = new APIClient("derp")
  })

  afterEach(() => {
    fetchMock.restore() // This actually resets everything
  });

  describe("Throws to 401", () => {
    it('on reading', async () => {
      mockWithCode(401)
      return expect(clientRead()).rejects.toBeTruthy()
    })

    it('on posting', async () => {
      mockWithCode(401)
      return expect(clientPost()).rejects.toBeTruthy()
    })

    it('on putting', async () => {
      mockWithCode(401)
      return expect(clientPut()).rejects.toBeTruthy()
    })
  })

  describe('Throws on error', () => {
    it('on reading', async () => {
      mockWithCode(400)
      return expect(clientRead()).rejects.toBeTruthy()
    });

    it('on posting', async () => {
      mockWithCode(400)
      return expect(clientPost()).rejects.toBeTruthy()
    });

    it('on putting', async () => {
      mockWithCode(400)
      return expect(clientPut()).rejects.toBeTruthy()
    });
  });

  function mockWithCode(code) {
    fetchMock.mock("*", code)
  }

  function clientRead(endpoint = "/") {
    return apiClient.readFrom(endpoint)
  }

  function clientPost(endpoint = "/") {
    return apiClient.postTo(endpoint)
  }

  function clientPut(endpoint = "/") {
    return apiClient.putTo(endpoint)
  }
})
